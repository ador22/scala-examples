Scala examples, partly from "Programming Scala" (2nd edition) book.
I started from copying the Akka example from [https://github.com/deanwampler/prog-scala-2nd-ed-code-examples](here).


```bash
$ cd akka/crud-example
$ sbt "run-main akkacrud.AkkaClient" < run-akka-input.txt
```
