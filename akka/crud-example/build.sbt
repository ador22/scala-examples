name := "akka-crud-example"

version := "0.8"

scalaVersion := "2.11.7"

libraryDependencies ++= {
  val akkaV = "2.4.2"
  Seq(
    "com.typesafe.akka"   %%  "akka-actor"      % akkaV,
    "com.typesafe.akka"   %%  "akka-testkit"    % akkaV    % "test",
    "org.scalacheck"      %%  "scalacheck"      % "1.13.0" % "test",
    "org.scalatest"       %%  "scalatest"       % "2.2.6"  % "test",
    "org.specs2"          %%  "specs2"          % "3.7"    % "test",
    "ch.qos.logback"      %   "logback-classic" % "1.1.6",
    "com.typesafe.akka"   %% "akka-slf4j"       % "2.4.2"
  )
}
